import { Controller, Body } from '@nestjs/common';
import { RMQValidate, RMQRoute } from 'nestjs-rmq';
import { AccountsUserInfo, AccountUserCourses } from '@microservices/contracts';
import { UserRepository } from './repositories/user.repository';


@Controller()
export class UserQueries{
    constructor(private readonly userRepository: UserRepository) {}
    @RMQValidate()
    @RMQRoute(AccountsUserInfo.topic)
    async userInfo(@Body() { id }: AccountsUserInfo.Request): Promise<AccountsUserInfo.Response> {
        const user = await this.userRepository.findUserById(id);
        // const profile = new UserEntity(user).getPublicProfile();
        return {
            user
        };
    }
    
    
    @RMQValidate()
    @RMQRoute(AccountUserCourses.topic)
    async userCourses(@Body() { id }: AccountUserCourses.Request): Promise<AccountUserCourses.Response> {
        const user = await this.userRepository.findUserById(id);
        return {
            courses: user.courses
        };
    }
}
