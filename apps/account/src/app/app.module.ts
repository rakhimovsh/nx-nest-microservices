import { Module } from '@nestjs/common';
import { RMQModule } from 'nestjs-rmq';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { getMongoConfig } from './configs/mongo.config';
import { getRMQConfig } from './configs/rmq.config';
import { UserCommands } from './user/user.commands';
import { UserQueries } from './user/user.queries';

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true, envFilePath: 'envs/.account.env' }),
        RMQModule.forRootAsync(getRMQConfig()),
        UserModule,
        AuthModule,
        MongooseModule.forRootAsync(getMongoConfig()),
    ],
    controllers: [UserCommands, UserQueries],
})
export class AppModule {}
